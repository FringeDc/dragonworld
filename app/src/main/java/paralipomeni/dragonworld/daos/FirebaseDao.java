package paralipomeni.dragonworld.daos;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.activities.Classifica;
import paralipomeni.dragonworld.activities.MyProfile;
import paralipomeni.dragonworld.activities.Radar;
import paralipomeni.dragonworld.activities.Registrati;
import paralipomeni.dragonworld.activities.SplashScreen;
import paralipomeni.dragonworld.activities.Statistiche;
import paralipomeni.dragonworld.dtos.UserDTO;
import paralipomeni.dragonworld.enums.FirebaseAccess;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.pojos.SingletonExtras;
import paralipomeni.dragonworld.populators.UserPopulator;
import paralipomeni.dragonworld.services.DatabaseService;
import paralipomeni.dragonworld.services.LangService;
import paralipomeni.dragonworld.services.UserService;

import static paralipomeni.dragonworld.constants.DragonConstants.ActivityConsts.EXTRA_BANNED;
import static paralipomeni.dragonworld.constants.DragonConstants.ActivityConsts.EXTRA_CHART_POSITION;
import static paralipomeni.dragonworld.constants.DragonConstants.Database.TABLE_FIELD_NICKNAME;
import static paralipomeni.dragonworld.constants.DragonConstants.Firebase.TREE_FIELD_USERS;

public class FirebaseDao {

    public static void execute(final FragmentActivity activity, final FirebaseAccess type, final Object... params) {

        final DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference(TREE_FIELD_USERS);
        final Context context = activity.getApplicationContext();

        mRootRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                FirebaseAccess subType = type;
                switch (subType) {
                    case CLASSIFICA: {
                        subType = FirebaseAccess.COMPLETED;
                        List<User> users = getAllSortedUsers(dataSnapshot);

                        SingletonExtras.get().setClassifica(users);

                        Intent intent = new Intent(activity, Classifica.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        break;
                    }
                    case STATISTICHE: {
                        subType = FirebaseAccess.COMPLETED;
                        List<User> users = getAllUsers(dataSnapshot);

                        SingletonExtras.get().setClassifica(users);

                        Intent intent = new Intent(activity, Statistiche.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        break;
                    }
                    case USER_PROFILE: {
                        subType = FirebaseAccess.COMPLETED;

                        String deviceID = UserService.getDeviceID(context);
                        Integer position = getUserPositionOnTheChart(dataSnapshot, deviceID);

                        Intent intent = new Intent(activity, MyProfile.class);
                        intent.putExtra(EXTRA_CHART_POSITION, position);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        break;
                    }
                    case UPDATE_USER: {
                        subType = FirebaseAccess.COMPLETED;
                        UserDTO userDTO = (UserDTO) params[0];
                        String deviceID = userDTO.getDeviceID();

                        userDTO.setToBeUploaded(false);
                        dataSnapshot.getRef().child(deviceID).setValue(userDTO);

                        User user = UserPopulator.userDTO2user(userDTO);
                        if(user != null && UserService.hasUserCheated(user)) {
                            Intent intent = new Intent(activity, SplashScreen.class);
                            DatabaseService.deleteDatabase(context);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            context.startActivity(intent);
                        } else {
                            DatabaseService.updateCurrentUser(context, userDTO);
                        }

                        activity.finish();
                        break;
                    }
                    case DOWNLOAD_USER: {
                        subType = FirebaseAccess.COMPLETED;
                        String deviceID = params[0].toString();
                        UserDTO userDTO = getUserByDeviceID(dataSnapshot, deviceID);

                        Intent intent = new Intent(activity, Registrati.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        //Controllo se l'utente ha barato nel momento in cui prova a recuperarsi l'utente.
                        //In caso, cancello

                        User user = UserPopulator.userDTO2user(userDTO);
                        if(user != null && UserService.hasUserCheated(user)) {
                            dataSnapshot.getRef().child(deviceID).removeValue();
                            intent.putExtra(EXTRA_BANNED, user.getNickname());
                            context.startActivity(intent);
                            activity.finish();
                            break;
                        }

                        if (userDTO != null) {
                            DatabaseService.updateCurrentUser(context, userDTO);
                            intent = new Intent(activity, Radar.class);
                        }
                        context.startActivity(intent);
                        activity.finish();
                        break;
                    }
                    case UPLOAD_USER: {
                        subType = FirebaseAccess.COMPLETED;
                        Intent intent = new Intent(activity, Radar.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        String deviceID = UserService.getDeviceID(context);

                        UserDTO preExistingUserDTO = getUserByDeviceID(dataSnapshot, deviceID);
                        if (preExistingUserDTO != null) {
                            DatabaseService.updateCurrentUser(context, preExistingUserDTO);
                            context.startActivity(intent);
                            break;
                        }

                        UserDTO userDTO = (UserDTO) params[0];
                        if(userDTO == null) {
                            break;
                        }

                        String newUserNickname = userDTO.getNickname();

                        boolean alredyUserNickname = false;
                        for (DataSnapshot dsp : dataSnapshot.getChildren()) {

                            String fbNickname = dsp.child(TABLE_FIELD_NICKNAME).getValue(String.class);
                            if (LangService.areEquals(fbNickname, newUserNickname)) {
                                Toast.makeText(context, context.getString(R.string.fetch_nickERR), Toast.LENGTH_LONG).show();
                                intent = new Intent(activity, Registrati.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                alredyUserNickname = true;
                                break;
                            }
                        }

                        if(!alredyUserNickname) {
                            DatabaseService.updateCurrentUser(context, userDTO);
                            dataSnapshot.getRef().child(deviceID).setValue(userDTO);
                        }
                        context.startActivity(intent);
                        break;
                    }
                }
                mRootRef.removeEventListener(this);
                Log.i("FIREBASE", "Firebase transaction completed with status: " + subType);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

    }

    private static UserDTO getUserByDeviceID(DataSnapshot dataSnapshot, @NonNull String deviceID) {
        return dataSnapshot.hasChild(deviceID) ?
                dataSnapshot.child(deviceID).getValue(UserDTO.class) : null;
    }

    private static List<User> getAllUsers(DataSnapshot dataSnapshot) {
        List<User> allUsers = new ArrayList<>();

        for (DataSnapshot dsp : dataSnapshot.getChildren()) {
            UserDTO userDTO = dsp.getValue(UserDTO.class);
            User user = UserPopulator.userDTO2user(userDTO);
            if (user != null) {
                allUsers.add(user);
            }
        }
        return allUsers;
    }

    private static List<User> getAllSortedUsers(DataSnapshot dataSnapshot) {
        List<User> allUsers = getAllUsers(dataSnapshot);
        Collections.sort(allUsers);
        return allUsers;
    }

    private static Integer getUserPositionOnTheChart(DataSnapshot dataSnapshot, String deviceID) {
        List<User> allUsers = getAllSortedUsers(dataSnapshot);

        for (User userChart : allUsers) {

            if (userChart.getDeviceID().equals(deviceID)) {
                return allUsers.indexOf(userChart) + 1;
            }
        }
        return null;
    }
}
