package paralipomeni.dragonworld.daos;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import paralipomeni.dragonworld.dtos.SferaDTO;
import paralipomeni.dragonworld.dtos.UserDTO;
import paralipomeni.dragonworld.populators.SferaPopulator;
import paralipomeni.dragonworld.populators.UserPopulator;

import static paralipomeni.dragonworld.constants.DragonConstants.Database.*;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.FALSE_VALUE;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.TRUE_VALUE;

public class DatabaseDao {

    private static SQLiteDatabase myData;

    public static void updateCurrentUser(Context context, UserDTO userDTO) {
        dropTables(context);

        if(userDTO == null) {
            return;
        }

        myData = openDataBase(context);
        String[] userParams = {
                userDTO.getNickname(),
                userDTO.getNazione(),
                userDTO.getDeviceID(),
                userDTO.getCitta(),
                userDTO.getInizio(),
                userDTO.getFine(),
                Boolean.TRUE.equals(userDTO.getToBeUploaded()) ? TRUE_VALUE : FALSE_VALUE,
                userDTO.getWish()};
        myData.execSQL(QUERY_INSERT_USER, userParams);

        List<SferaDTO> sfereDTO = userDTO.getSfere();
        if(sfereDTO == null) {
            return;
        }

        for(SferaDTO sferaDTO : sfereDTO) {
            String[] sferaParams = {
                    Boolean.TRUE.equals(sferaDTO.getFound()) ? TRUE_VALUE : FALSE_VALUE,
                    String.valueOf(sferaDTO.getNumeroSfera()),
                    String.valueOf(sferaDTO.getLatitude()),
                    String.valueOf(sferaDTO.getLongitude())};
            myData.execSQL(QUERY_INSERT_SFERA, sferaParams);
        }
        myData.close();
    }

    public static UserDTO getCurrentUser(Context context) {
        UserDTO userDTO = null;
        myData = openDataBase(context);
        Cursor cursor = myData.rawQuery(QUERY_SELECT_USER, null);

        if (cursor.moveToFirst()) {
            userDTO = UserPopulator.cursor2userDTO(cursor);
        }
        if(userDTO == null) {
            dropTables(context);
            return null;
        }

        cursor = myData.rawQuery(QUERY_SELECT_SFERA, null);

        List<SferaDTO> sfereDTO = new ArrayList<>();
        while (cursor.moveToNext()) {
            SferaDTO sferaDTO = SferaPopulator.cursor2sferaDTO(cursor);
            sfereDTO.add(sferaDTO);
        }
        userDTO.setSfere(sfereDTO);
        cursor.close();
        myData.close();

        return userDTO;
    }

    private static void dropTables(Context context) {
        myData = context.openOrCreateDatabase(DB_NAME, Context.MODE_PRIVATE, null);
        myData.execSQL(QUERY_DELETE_TABLE_USER);
        myData.execSQL(QUERY_DELETE_TABLE_SFERE);
        myData.close();
    }

    private static SQLiteDatabase openDataBase(Context context) {
        myData = context.openOrCreateDatabase(DB_NAME, Context.MODE_PRIVATE, null);
        myData.execSQL(QUERY_CREATE_TABLE_USER);
        myData.execSQL(QUERY_CREATE_TABLE_SFERE);
        return myData;
    }

    public static void deleteDatabase(Context context) {
        myData = context.openOrCreateDatabase(DB_NAME, Context.MODE_PRIVATE, null);
        myData.close();
        context.deleteDatabase(DB_NAME);
    }
}
