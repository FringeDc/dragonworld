package paralipomeni.dragonworld.validators;

import android.content.Context;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.exceptions.DragonWorldValidationException;

import static paralipomeni.dragonworld.constants.DragonConstants.RegEx.REGEX_CLEARCHARS;
import static paralipomeni.dragonworld.constants.DragonConstants.RegEx.REGEX_ONLYNUM;

public class InputValidator {

    public static void validateNickname(Context context, String nickname) throws DragonWorldValidationException {
        Pattern pattern = Pattern.compile(REGEX_CLEARCHARS);
        Matcher matcher = pattern.matcher(nickname);
        boolean match = matcher.find();

        if (nickname.length() < 6) {
            throw new DragonWorldValidationException(context.getString(R.string.reg_minNameERR));
        } else if (nickname.length() > 20) {
            throw new DragonWorldValidationException(context.getString(R.string.reg_maxNameERR));
        } else if (!match || matcher.group(0).length() < nickname.length()) {
            throw new DragonWorldValidationException(context.getString(R.string.reg_compNameERR));
        }
    }

    public static void validateCitta(Context context, String citta) throws DragonWorldValidationException {
        Pattern pattern = Pattern.compile(REGEX_ONLYNUM);
        Matcher matcher = pattern.matcher(citta);

        if (matcher.find()) {
            throw new DragonWorldValidationException(context.getString(R.string.reg_numCityERR));
        } else if (citta.length() == 0) {
            throw new DragonWorldValidationException(context.getString(R.string.reg_cityERR));
        }
    }

    public static void validateNazione(Context context, String nazione) throws DragonWorldValidationException {
        Pattern pattern = Pattern.compile(REGEX_ONLYNUM);
        Matcher matcher = pattern.matcher(nazione);

        if (matcher.find()) {
            throw new DragonWorldValidationException(context.getString(R.string.reg_numCountryERR));
        } else if (nazione.length() == 0) {
            throw new DragonWorldValidationException(context.getString(R.string.reg_countryERR));
        }
    }
}
