package paralipomeni.dragonworld.dtos;

import java.io.Serializable;

public class SferaDTO implements Serializable
{
    private Boolean found;
    private Double latitude;
    private Double longitude;
    private Integer numeroSfera;

    public Boolean getFound() {
        return found;
    }

    public void setFound(Boolean found) {
        this.found = found;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getNumeroSfera() {
        return numeroSfera;
    }

    public void setNumeroSfera(Integer numeroSfera) {
        this.numeroSfera = numeroSfera;
    }
}
