package paralipomeni.dragonworld.dtos;

import java.util.List;

public class UserDTO {

    private String inizio;
    private String fine;
    private String nickname;
    private String citta;
    private String nazione;
    private String deviceID;
    private String wish;
    private Boolean toBeUploaded;
    private List<SferaDTO> sfere;

    public String getInizio() {
        return inizio;
    }

    public void setInizio(String inizio) {
        this.inizio = inizio;
    }

    public String getFine() {
        return fine;
    }

    public void setFine(String fine) {
        this.fine = fine;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getNazione() {
        return nazione;
    }

    public void setNazione(String nazione) {
        this.nazione = nazione;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public List<SferaDTO> getSfere() {
        return sfere;
    }

    public void setSfere(List<SferaDTO> sfere) {
        this.sfere = sfere;
    }

    public Boolean getToBeUploaded() {
        return toBeUploaded;
    }

    public void setToBeUploaded(Boolean toBeUploaded) {
        this.toBeUploaded = toBeUploaded;
    }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }
}
