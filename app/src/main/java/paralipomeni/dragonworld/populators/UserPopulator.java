package paralipomeni.dragonworld.populators;

import android.database.Cursor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import paralipomeni.dragonworld.models.Sfera;
import paralipomeni.dragonworld.dtos.SferaDTO;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.dtos.UserDTO;

import static paralipomeni.dragonworld.constants.DragonConstants.Database.TABLE_FIELD_CITTA;
import static paralipomeni.dragonworld.constants.DragonConstants.Database.TABLE_FIELD_DEVICEID;
import static paralipomeni.dragonworld.constants.DragonConstants.Database.TABLE_FIELD_FINE;
import static paralipomeni.dragonworld.constants.DragonConstants.Database.TABLE_FIELD_INIZIO;
import static paralipomeni.dragonworld.constants.DragonConstants.Database.TABLE_FIELD_NAZIONE;
import static paralipomeni.dragonworld.constants.DragonConstants.Database.TABLE_FIELD_NICKNAME;
import static paralipomeni.dragonworld.constants.DragonConstants.Database.TABLE_FIELD_UPLOAD;
import static paralipomeni.dragonworld.constants.DragonConstants.Database.TABLE_FIELD_WISH;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.COMPLEX_DATE_FORMAT;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.TRUE_VALUE;

public class UserPopulator {

    public static User userDTO2user(UserDTO userDTO) {
        if(userDTO == null) {
            return null;
        }
        User user = new User();
        user.setNickname(userDTO.getNickname());
        user.setCitta(userDTO.getCitta());
        user.setDeviceID(userDTO.getDeviceID());
        user.setNazione(userDTO.getNazione());
        user.setWish(userDTO.getWish());
        user.setToBeUploaded(userDTO.getToBeUploaded());

        SimpleDateFormat sdf = new SimpleDateFormat(COMPLEX_DATE_FORMAT, Locale.getDefault());

        if(userDTO.getInizio() != null) {
            Date inizio = null;
            try {
                inizio = sdf.parse(userDTO.getInizio());
            } catch (ParseException ignored) {}
            user.setInizio(inizio);
        }

        if(userDTO.getFine() != null) {
            Date fine = null;
            try {
                fine = sdf.parse(userDTO.getFine());
            } catch (ParseException ignored) {}
            user.setFine(fine);
        }

        if(userDTO.getSfere() != null) {
            List<Sfera> sfere = new ArrayList<>();
            for (SferaDTO sferaDTO : userDTO.getSfere()) {
                sfere.add(SferaPopulator.sferaDTO2sfera(sferaDTO));
            }
            user.setSfere(sfere);
        }
        return user;
    }

    public static UserDTO user2userDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setNickname(user.getNickname());
        userDTO.setCitta(user.getCitta());
        userDTO.setDeviceID(user.getDeviceID());
        userDTO.setNazione(user.getNazione());
        userDTO.setWish(user.getWish());
        userDTO.setToBeUploaded(user.getToBeUploaded());

        SimpleDateFormat sdf = new SimpleDateFormat(COMPLEX_DATE_FORMAT, Locale.getDefault());

        if(user.getInizio() != null) {
            userDTO.setInizio(sdf.format(user.getInizio()));
        }
        if(user.getFine() != null) {
            userDTO.setFine(sdf.format(user.getFine()));
        }
        if(user.getSfere() != null) {
            List<SferaDTO> sfereDTO = new ArrayList<>();
            for (Sfera sfera : user.getSfere()) {
                sfereDTO.add(SferaPopulator.sfera2sferaDTO(sfera));
            }
            userDTO.setSfere(sfereDTO);
        }
        return userDTO;
    }

    public static UserDTO cursor2userDTO(Cursor c) {
        UserDTO userDTO = new UserDTO();

        int columnIndex = c.getColumnIndex(TABLE_FIELD_NICKNAME);
        if (columnIndex != -1) {
            userDTO.setNickname(c.getString(columnIndex));
        }
        columnIndex = c.getColumnIndex(TABLE_FIELD_NAZIONE);
        if (columnIndex != -1) {
            userDTO.setNazione(c.getString(columnIndex));
        }
        columnIndex = c.getColumnIndex(TABLE_FIELD_DEVICEID);
        if (columnIndex != -1) {
            userDTO.setDeviceID(c.getString(columnIndex));
        }
        columnIndex = c.getColumnIndex(TABLE_FIELD_CITTA);
        if (columnIndex != -1) {
            userDTO.setCitta(c.getString(columnIndex));
        }
        columnIndex = c.getColumnIndex(TABLE_FIELD_INIZIO);
        if (columnIndex != -1) {
            userDTO.setInizio(c.getString(columnIndex));
        }
        columnIndex = c.getColumnIndex(TABLE_FIELD_WISH);
        if (columnIndex != -1) {
            userDTO.setWish(c.getString(columnIndex));
        }
        columnIndex = c.getColumnIndex(TABLE_FIELD_FINE);
        if (columnIndex != -1) {
            userDTO.setFine(c.getString(columnIndex));
        }
        columnIndex = c.getColumnIndex(TABLE_FIELD_UPLOAD);
        if (columnIndex != -1) {
            String toBeUploaded = c.getString(columnIndex);
            userDTO.setToBeUploaded(toBeUploaded.equals(TRUE_VALUE));
        }

        if(userDTO.getDeviceID() == null || userDTO.getNickname() == null) {
            userDTO = null;
        }
        return userDTO;
    }

}
