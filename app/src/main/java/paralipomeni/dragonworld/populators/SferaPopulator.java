package paralipomeni.dragonworld.populators;

import android.database.Cursor;

import com.google.android.gms.maps.model.LatLng;

import paralipomeni.dragonworld.models.Sfera;
import paralipomeni.dragonworld.dtos.SferaDTO;

import static paralipomeni.dragonworld.constants.DragonConstants.Database.TABLE_FIELD_FOUND;
import static paralipomeni.dragonworld.constants.DragonConstants.Database.TABLE_FIELD_LAT;
import static paralipomeni.dragonworld.constants.DragonConstants.Database.TABLE_FIELD_LONG;
import static paralipomeni.dragonworld.constants.DragonConstants.Database.TABLE_FIELD_NUMERO;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.TRUE_VALUE;

public class SferaPopulator {

    public static Sfera sferaDTO2sfera(SferaDTO sferaDTO) {
        if(sferaDTO == null) {
            return null;
        }
        Sfera sfera = new Sfera();
        sfera.setNumeroSfera(sferaDTO.getNumeroSfera());

        boolean found = sferaDTO.getFound() != null && sferaDTO.getFound();
        sfera.setFound(found);

        LatLng latlng = new LatLng(sferaDTO.getLatitude(), sferaDTO.getLongitude());
        sfera.setLatlng(latlng);
        return sfera;
    }

    public static SferaDTO sfera2sferaDTO(Sfera sfera) {
        if(sfera == null) {
            return null;
        }
        SferaDTO sferaDTO = new SferaDTO();

        sferaDTO.setNumeroSfera(sfera.getNumeroSfera());
        sferaDTO.setFound(sfera.isFound());

        LatLng latlng = sfera.getLatlng();
        if(latlng != null) {
            sferaDTO.setLongitude(latlng.longitude);
            sferaDTO.setLatitude(latlng.latitude);
        }
        return sferaDTO;
    }

    public static SferaDTO cursor2sferaDTO(Cursor c) {
        SferaDTO sferaDTO = new SferaDTO();

        int columnIndex = c.getColumnIndex(TABLE_FIELD_FOUND);
        if (columnIndex != -1) {
            String found = c.getString(columnIndex);
            sferaDTO.setFound(found.equals(TRUE_VALUE));
        }
        columnIndex = c.getColumnIndex(TABLE_FIELD_NUMERO);
        if (columnIndex != -1) {
            sferaDTO.setNumeroSfera(c.getInt(columnIndex));
        }
        columnIndex = c.getColumnIndex(TABLE_FIELD_LONG);
        if (columnIndex != -1) {
            sferaDTO.setLongitude(c.getDouble(columnIndex));
        }
        columnIndex = c.getColumnIndex(TABLE_FIELD_LAT);
        if (columnIndex != -1) {
            sferaDTO.setLatitude(c.getDouble(columnIndex));
        }
        return sferaDTO;
    }
}
