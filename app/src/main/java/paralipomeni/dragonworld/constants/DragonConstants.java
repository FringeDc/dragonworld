package paralipomeni.dragonworld.constants;

public class DragonConstants {

    public interface Strings {
        String EMPTY = "";
        String CIRCLE = "°";
        String DASH = "-";
        String TRUE_VALUE = "1";
        String FALSE_VALUE = "0";
        String COMPLEX_DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
        String SIMPLE_DATE_FORMAT = "dd/MM/yyyy";
    }

    public interface Map {
        int MAX_ZOOM = 19;
        int MIN_ZOOM = 11;
        double SCREEN_MARGIN = 10;
    }

    public interface ActivityConsts {
        String EXTRA_CHART_POSITION = "EXTRA_CHART_POSITION";
        String EXTRA_SFERA_VICINA = "EXTRA_SFERA_VICINA";
        String EXTRA_BANNED = "EXTRA_BANNED";
    }

    public interface Dragonworld {
        int PERMISSIONS_REQUEST_LOCATION = 99;
        int DEFAULT_RESOURCE = -1;
        int LOCATION_MAX_ATTEMPTS = 10;
        long MILLIS_IN_A_DAY = 1000 * 60 * 60 * 24;
        int RAGGIO_DISTANZA_SFERA = 1000000000;
        int DAY_FOR_UNACTIVITY = 15;
    }

    public interface RegEx {
        String REGEX_CLEARCHARS = "[a-zA-Z0-9]+";
        String REGEX_ONLYNUM = "[0-9]";
    }

    public interface Firebase {
        String TREE_FIELD_USERS = "users";
    }

    public interface Database {

        String DB_NAME = "myData.db";

        String TABLE_USER = "user";
        String TABLE_SFERE = "sfere";

        String TABLE_FIELD_NICKNAME = "nickname";
        String TABLE_FIELD_NAZIONE = "nazione";
        String TABLE_FIELD_DEVICEID = "deviceid";
        String TABLE_FIELD_CITTA = "citta";
        String TABLE_FIELD_INIZIO = "inizio";
        String TABLE_FIELD_FINE = "fine";
        String TABLE_FIELD_WISH = "wish";
        String TABLE_FIELD_UPLOAD = "to_be_uploaded";

        String TABLE_FIELD_FOUND = "found";
        String TABLE_FIELD_NUMERO = "numerosfera";
        String TABLE_FIELD_LONG = "longitude";
        String TABLE_FIELD_LAT = "latitude";

        String QUERY_SELECT_USER = "SELECT * FROM '" + TABLE_USER + "'";
        String QUERY_SELECT_SFERA = "SELECT * FROM '" + TABLE_SFERE + "'";

        String QUERY_CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS " + TABLE_USER + " (" +
                TABLE_FIELD_NICKNAME + " text NOT NULL, " +
                TABLE_FIELD_NAZIONE + " text NOT NULL, " +
                TABLE_FIELD_DEVICEID + " text PRIMARY KEY, " +
                TABLE_FIELD_CITTA + " text NOT NULL, " +
                TABLE_FIELD_INIZIO + " text NOT NULL, " +
                TABLE_FIELD_WISH + " text, " +
                TABLE_FIELD_UPLOAD + " integer DEFAULT 1, " +
                TABLE_FIELD_FINE + " text);";

        String QUERY_CREATE_TABLE_SFERE = "CREATE TABLE IF NOT EXISTS " + TABLE_SFERE + " (" +
                TABLE_FIELD_FOUND + " integer DEFAULT 0, " +
                TABLE_FIELD_NUMERO + " integer PRIMARY KEY, " +
                TABLE_FIELD_LONG + " real NOT NULL, " +
                TABLE_FIELD_LAT + " real NOT NULL);";

        String QUERY_DELETE_TABLE_USER = "DROP TABLE IF EXISTS '" + TABLE_USER + "';";
        String QUERY_DELETE_TABLE_SFERE = "DROP TABLE IF EXISTS '" + TABLE_SFERE + "';";

        String QUERY_INSERT_USER = "INSERT INTO " + TABLE_USER + "(" +
                TABLE_FIELD_NICKNAME + ", " +
                TABLE_FIELD_NAZIONE + ", " +
                TABLE_FIELD_DEVICEID + ", " +
                TABLE_FIELD_CITTA + ", " +
                TABLE_FIELD_INIZIO + ", " +
                TABLE_FIELD_FINE + ", " +
                TABLE_FIELD_UPLOAD + ", " +
                TABLE_FIELD_WISH + ") VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        String QUERY_INSERT_SFERA = "INSERT INTO " + TABLE_SFERE + "(" +
                TABLE_FIELD_FOUND + ", " +
                TABLE_FIELD_NUMERO + ", " +
                TABLE_FIELD_LAT + ", " +
                TABLE_FIELD_LONG + ") VALUES (?, ?, ?, ?)";
    }
}
