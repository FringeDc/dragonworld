package paralipomeni.dragonworld.services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.List;

import paralipomeni.dragonworld.R;

import static paralipomeni.dragonworld.constants.DragonConstants.Dragonworld.LOCATION_MAX_ATTEMPTS;
import static paralipomeni.dragonworld.constants.DragonConstants.Map.SCREEN_MARGIN;

public class MapService {

    public static double distanza(LatLng a, LatLng b) {
        float[] distance = new float[1];
        Location.distanceBetween(a.latitude, a.longitude, b.latitude, b.longitude, distance);
        return distance[0];
    }

    public static LatLng getArrowPoint(LatLng sfera, LatLng centro, double raggio) {

        //CONTROLLATO SU GOOGLE MAPS:
        //longitude: asse X
        //latitude: asse Y

        double x1 = sfera.longitude;
        double y1 = sfera.latitude;
        double x2 = centro.longitude;
        double y2 = centro.latitude;

        double off_x = x1 - x2;
        double off_y = y1 - y2;

        double ls = off_x * off_x + off_y * off_y;

        if (ls < raggio * raggio)
            return null;

        double scale = raggio / Math.sqrt(ls);
        scale -= (SCREEN_MARGIN / 100) * scale;
        double res_x = off_x * scale + x2;
        double res_y = off_y * scale + y2;
        return new LatLng(res_y, res_x);
    }

    public static double getRadiusDegrees(GoogleMap mMap) {
        LatLng centro = mMap.getCameraPosition().target;
        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        return bounds.northeast.longitude - centro.longitude;
    }

    private static LocationListener getLocationListener() {
        return new LocationListener() {
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }

            @Override
            public void onLocationChanged(final Location location) {
            }
        };
    }

    @SuppressLint("MissingPermission")
    public static LatLng getGPSPos(Context context) {
        if(!isGPSEnabled(context)) {
            return null;
        }

        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if(locationManager == null) {
            return null;
        }
        Location location = null;

        List<String> providers = locationManager.getAllProviders();
        for (String prov : providers) {
            locationManager.requestLocationUpdates( prov, 0, 0, getLocationListener());
            location = locationManager.getLastKnownLocation(prov);
            for (int k = 0; k < LOCATION_MAX_ATTEMPTS; k++) {
                location = locationManager.getLastKnownLocation(prov);
            }

            if (location != null) break;
        }

        if (location != null)
            return new LatLng(location.getLatitude(), location.getLongitude());
        else
            return null;
    }

    public static boolean isGPSEnabled(final Context context) {

        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (manager == null || !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(context, context.getString(R.string.radar_enableGPS), Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }
}
