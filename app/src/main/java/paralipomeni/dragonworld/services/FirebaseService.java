package paralipomeni.dragonworld.services;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import java.util.Date;
import java.util.List;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.daos.FirebaseDao;
import paralipomeni.dragonworld.dtos.UserDTO;
import paralipomeni.dragonworld.enums.FirebaseAccess;
import paralipomeni.dragonworld.models.Sfera;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.populators.UserPopulator;

import static paralipomeni.dragonworld.constants.DragonConstants.Dragonworld.MILLIS_IN_A_DAY;

public class FirebaseService {

    public static boolean updateUser(FragmentActivity activity, User user) {
        Context context = activity.getApplicationContext();

        if (UserService.isUserInvalid(user)) {
            return false;
        }

        UserDTO userDTO = UserPopulator.user2userDTO(user);

        if (!ConnectionService.isNewtworkAvailable(context)) {
            return false;
        }
        FirebaseDao.execute(activity, FirebaseAccess.UPDATE_USER, userDTO);

        //In caso in cui l'utente abbia barato, ragiono così:
        //cancello il DB in locale
        //Quando va a ripescarselo da Internet, faccio il check se Pippo ha barato
        //così da assicurarmi che abbia l'accesso.

        //Se ha barato, quando prova a ripescarselo, glielo cancello anche da Firebase
        //Quindi, per ora, glielo cancello solo in locale

        int sfereRaccolte = UserService.getQuanteSfereRaccolte(user);
        if(sfereRaccolte == 7) {
            long inizio = user.getInizio().getTime();
            long fine = user.getFine() == null ? new Date().getTime() : user.getFine().getTime();

            if(fine - inizio <= MILLIS_IN_A_DAY) {
                DatabaseService.deleteDatabase(context);
                return true;
            }
        }

        DatabaseService.updateCurrentUser(context, userDTO);
        return true;
    }

    public static void uploadNewUser(FragmentActivity activity, User user) {
        Context context = activity.getApplicationContext();

        if (UserService.isUserInvalid(user)) {
            return;
        }
        if (!ConnectionService.isNewtworkAvailable(context)) {
            Toast.makeText(context, context.getString(R.string.MU_conn_unavailable), Toast.LENGTH_LONG).show();
            return;
        }

        UserDTO userDTO = UserPopulator.user2userDTO(user);

        FirebaseDao.execute(activity, FirebaseAccess.UPLOAD_USER, userDTO);
    }

    public static void downloadUser(FragmentActivity activity) {
        Context context = activity.getApplicationContext();
        String deviceID = UserService.getDeviceID(context);

        if (ConnectionService.isNewtworkAvailable(context)) {
            FirebaseDao.execute(activity, FirebaseAccess.DOWNLOAD_USER, deviceID);
        } else {
            Toast.makeText(context, context.getString(R.string.MU_conn_unavailable), Toast.LENGTH_LONG).show();
        }
    }

    public static void getUserProfile(FragmentActivity activity) {
        Context context = activity.getApplicationContext();

        if (!ConnectionService.isNewtworkAvailable(context)) {
            Toast.makeText(context, context.getString(R.string.MU_conn_unavailable), Toast.LENGTH_LONG).show();
            return;
        }

        FirebaseDao.execute(activity, FirebaseAccess.USER_PROFILE);
    }

    public static void mostraClassifica(FragmentActivity activity) {
        Context context = activity.getApplicationContext();
        if (!ConnectionService.isNewtworkAvailable(context)) {
            Toast.makeText(context, context.getString(R.string.MU_conn_unavailable), Toast.LENGTH_LONG).show();
            return;
        }
        FirebaseDao.execute(activity, FirebaseAccess.CLASSIFICA);
    }

    public static void mostraStatitiche(FragmentActivity activity) {
        Context context = activity.getApplicationContext();
        if (!ConnectionService.isNewtworkAvailable(context)) {
            Toast.makeText(context, context.getString(R.string.MU_conn_unavailable), Toast.LENGTH_LONG).show();
            return;
        }
        FirebaseDao.execute(activity, FirebaseAccess.STATISTICHE);
    }
}
