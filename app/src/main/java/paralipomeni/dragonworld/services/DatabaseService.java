package paralipomeni.dragonworld.services;

import android.content.Context;

import java.util.Date;
import java.util.List;

import paralipomeni.dragonworld.daos.DatabaseDao;
import paralipomeni.dragonworld.daos.FirebaseDao;
import paralipomeni.dragonworld.dtos.UserDTO;
import paralipomeni.dragonworld.enums.FirebaseAccess;
import paralipomeni.dragonworld.models.Sfera;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.populators.UserPopulator;

import static paralipomeni.dragonworld.constants.DragonConstants.Dragonworld.MILLIS_IN_A_DAY;

public class DatabaseService {

    public static User getCurrentUser(Context context) {
        UserDTO userDTO = DatabaseDao.getCurrentUser(context);

        if (userDTO == null) {
            return null;
        }
        return UserPopulator.userDTO2user(userDTO);
    }

    public static void updateCurrentUser(Context context, UserDTO userDTO) {
        if(userDTO == null) {
            return;
        }
        DatabaseDao.updateCurrentUser(context, userDTO);
    }

    public static void deleteDatabase(Context context) {
        DatabaseDao.deleteDatabase(context);
    }
}
