package paralipomeni.dragonworld.services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.provider.Settings;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.Date;
import java.util.Locale;

import paralipomeni.dragonworld.models.Sfera;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.populators.UserPopulator;

import static paralipomeni.dragonworld.constants.DragonConstants.Dragonworld.DAY_FOR_UNACTIVITY;
import static paralipomeni.dragonworld.constants.DragonConstants.Dragonworld.MILLIS_IN_A_DAY;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.EMPTY;

public class UserService {

    public static boolean isUserInvalid(User user) {
        return user == null
                || user.getDeviceID() == null
                || user.getDeviceID().isEmpty()
                || user.getNickname() == null
                || user.getNickname().isEmpty();
    }

    public static boolean isCurrentUserLoggedIn(Context context) {
        User user = DatabaseService.getCurrentUser(context);
        String deviceID = getDeviceID(context);

        return !isUserInvalid(user) && deviceID != null && !deviceID.isEmpty() && deviceID.equals(user.getDeviceID());
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static LatLng getPosizioneCitta(Context context, User user) {
        String citta = user.getCitta();

        Locale loc = new Locale(EMPTY, user.getNazione());
        String country = loc.getDisplayCountry();

        try {
            Geocoder geoCoder = new Geocoder(context, loc);
            Address address = geoCoder.getFromLocationName(country + ", " + citta, 1).get(0);
            return new LatLng(address.getLatitude(), address.getLongitude());
        } catch (IOException ex) {
            return null;
        }
    }

    public static int getQuanteSfereRaccolte(User user) {
        int raccolte = 0;
        if(user == null || user.getSfere() == null) {
            return raccolte;
        }
        for(Sfera sfera : user.getSfere()) {
            raccolte = sfera.isFound() ? raccolte+1 : raccolte;
        }
        return raccolte;
    }

    private static long getTempoDiGioco(User user) {
        long start = (user.getInizio() == null ? new Date() : user.getInizio()).getTime();
        long end = (user.getFine() == null ? new Date() : user.getFine()).getTime();

        return Math.max(0, end - start);
    }

    public static int getGiorniDiGioco(User user) {
        long millis = getTempoDiGioco(user);
        return (int) (millis/MILLIS_IN_A_DAY);
    }

    public static boolean isUserInactive(User user) {
        return getQuanteSfereRaccolte(user) == 0 && getGiorniDiGioco(user) > DAY_FOR_UNACTIVITY;
    }

    public static boolean hasUserCheated(User user) {
        int sfereRaccolte = getQuanteSfereRaccolte(user);

        if(sfereRaccolte == 7) {
            long inizio = user.getInizio().getTime();
            long fine = user.getFine() == null ? new Date().getTime() : user.getFine().getTime();
            return fine - inizio <= MILLIS_IN_A_DAY;
        }
        return false;
    }


}
