package paralipomeni.dragonworld.services;

import android.content.Context;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import paralipomeni.dragonworld.models.Sfera;
import paralipomeni.dragonworld.dtos.SferaDTO;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.populators.SferaPopulator;

import static paralipomeni.dragonworld.constants.DragonConstants.Dragonworld.RAGGIO_DISTANZA_SFERA;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.EMPTY;

public class SfereService {

    public static List<Sfera> getSfereOnRandomLocation(LatLng point, int radius) {

        List<Sfera> randomSfere = new ArrayList<>();
        Location myLocation = new Location(EMPTY);

        double lat = point.latitude;
        double lon = point.longitude;

        myLocation.setLatitude(lat);
        myLocation.setLongitude(lon);

        // Convert radius from meters to degrees
        double radiusInDegrees = radius / 111000f;

        Random r = new Random();

        //This is to generate 7 random points
        for(int i = 1; i<8; i++) {
            double x = radiusInDegrees * Math.sqrt(r.nextDouble()) * Math.cos(2 * Math.PI * r.nextDouble()) / Math.cos(lon);
            double y = radiusInDegrees * Math.sqrt(r.nextDouble()) * Math.sin(2 * Math.PI * r.nextDouble());

            Sfera sfera = new Sfera();
            sfera.setNumeroSfera(i);
            sfera.setFound(false);
            sfera.setLatlng(new LatLng(x + lat, y + lon));
            randomSfere.add(sfera);
        }

        return randomSfere;
    }

    public static SferaDTO sferaPiuVicina(Context context) {
        LatLng myPos = MapService.getGPSPos(context);

        if (myPos == null) {
            return null;
        }

        User user = DatabaseService.getCurrentUser(context);
        if (user == null) {
            return null;
        }

        List<Sfera> sfere = user.getSfere();
        Sfera sferaPiuVicina = null;

        float minDistanza = -1;

        for (Sfera sfera : sfere) {

            LatLng sferaPos = sfera.getLatlng();

            float[] d = new float[1];
            Location.distanceBetween(myPos.latitude, myPos.longitude,
                    sferaPos.latitude, sferaPos.longitude, d);
            float distance = d[0];

            if ((minDistanza == -1 || distance < minDistanza) && !sfera.isFound()) {
                minDistanza = distance;
                sferaPiuVicina = sfera;
            }
        }

        if (minDistanza < RAGGIO_DISTANZA_SFERA) {
            return SferaPopulator.sfera2sferaDTO(sferaPiuVicina);
        } else {
            return null;
        }
    }
}
