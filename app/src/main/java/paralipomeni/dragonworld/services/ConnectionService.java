package paralipomeni.dragonworld.services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import paralipomeni.dragonworld.BuildConfig;
import paralipomeni.dragonworld.R;

public class ConnectionService {

    public static boolean isNewtworkAvailable(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm == null) {
            return false;
        }
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static InterstitialAd initializeAdInterstitial(Context context) {
        MobileAds.initialize(context, context.getResources().getString(R.string.ADMOB_APP_ID));

        final InterstitialAd mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(BuildConfig.InterstitialAd);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        return mInterstitialAd;
    }

    public static void showAdInterstitial(InterstitialAd mInterstitialAd) {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.w("ADMOB", "The interstitial was not loaded yet.");
        }
    }
}
