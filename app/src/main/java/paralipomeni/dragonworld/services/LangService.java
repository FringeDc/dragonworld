package paralipomeni.dragonworld.services;

public class LangService {

    public static boolean areEquals(String fbNickname, String newUserNickname) {
        fbNickname = fbNickname.toLowerCase();
        newUserNickname = newUserNickname.toLowerCase();
        return fbNickname.equals(newUserNickname);
    }
}
