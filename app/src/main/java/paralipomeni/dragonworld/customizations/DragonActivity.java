package paralipomeni.dragonworld.customizations;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import paralipomeni.dragonworld.R;

@SuppressLint("Registered")
public class DragonActivity extends AppCompatActivity
{
    public void toast(String txt) {
        Toast t = Toast.makeText(getApplicationContext(), txt, Toast.LENGTH_LONG);
        TextView tv = t.getView().findViewById(android.R.id.message);
        tv.setGravity(Gravity.CENTER);
        t.show();
    }

    protected void toast(int resource) {
        toast(getString(resource));
    }

    protected void toast(int resource, Object... args) {
        String text = String.format(getString(resource), args);
        toast(text);
    }

    public void toastNoConnection() {
        toast(getString(R.string.MU_conn_unavailable));
    }

}
