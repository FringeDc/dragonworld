package paralipomeni.dragonworld.customizations;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.activities.SplashScreen;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.pojos.DragonHandler;
import paralipomeni.dragonworld.pojos.FrecciaSuMappaList;
import paralipomeni.dragonworld.services.DatabaseService;

@SuppressLint("Registered")
public class DragonFragmentActivity extends FragmentActivity {
    private FrecciaSuMappaList arrowList;
    private User currentUser;
    protected Marker myPosition;
    protected LatLng myPos;

    protected DragonHandler handlerSfere;

    public void toast(String txt) {
        Toast t = Toast.makeText(getApplicationContext(), txt, Toast.LENGTH_LONG);
        TextView tv = t.getView().findViewById(android.R.id.message);
        tv.setGravity(Gravity.CENTER);
        t.show();
    }

    protected void toast(int resource) {
        toast(getString(resource));
    }

    protected void toast(int resource, Object... args) {
        String text = String.format(getString(resource), args);
        toast(text);
    }

    protected void toastNoConnection() {
        toast(getString(R.string.MU_conn_unavailable));
    }

    protected User currentUser() {
        if (currentUser == null) {
            currentUser = DatabaseService.getCurrentUser(getApplicationContext());

            if (currentUser == null) {
                Intent intent = new Intent(getApplicationContext(), SplashScreen.class);
                startActivity(intent);
                finish();
                //per evitare i nullPointer warning sul codice
                return new User();
            }
        }
        return currentUser;
    }

    protected FrecciaSuMappaList getFrecceSuMappa() {
        if (arrowList == null) {
            arrowList = new FrecciaSuMappaList();
        }
        return arrowList;
    }

    protected void removeMyPosition() {
        if(myPosition != null) {
            myPosition.remove();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        killAllThreads();
    }

    @Override
    public void onStop() {
        super.onStop();
        killAllThreads();
    }

    private void killAllThreads() {
        if (handlerSfere != null) {
            handlerSfere.kill();
        }
    }
}

