package paralipomeni.dragonworld.enums;

public enum FirebaseAccess
{
    CLASSIFICA,
    USER_PROFILE,
    UPDATE_USER,
    UPLOAD_USER,
    DOWNLOAD_USER,
    STATISTICHE,
    COMPLETED
}
