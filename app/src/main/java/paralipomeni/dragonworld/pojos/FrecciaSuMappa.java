package paralipomeni.dragonworld.pojos;

import com.google.android.gms.maps.model.Marker;

import paralipomeni.dragonworld.models.Sfera;

public class FrecciaSuMappa {

    private Sfera sfera;
    private Marker marker;

    public FrecciaSuMappa(Sfera sfera, Marker marker) {
        this.sfera = sfera;
        this.marker = marker;
    }

    public Sfera getSfera() {
        return sfera;
    }

    public void setSfera(Sfera sfera) {
        this.sfera = sfera;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }
}
