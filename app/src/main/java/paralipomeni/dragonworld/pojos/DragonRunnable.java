package paralipomeni.dragonworld.pojos;

import com.google.android.gms.maps.model.Marker;

import java.util.List;

public abstract class DragonRunnable implements Runnable {

    private List<Marker> markers;

    //sfere lampeggianti
    public DragonRunnable(List<Marker> markers) {
        super();
        this.markers = markers;
    }

    private boolean isLamped = true;

    public boolean isLamped() {
        return isLamped;
    }

    public void setLamped(boolean lamped) {
        isLamped = lamped;
    }

    public List<Marker> getMarkers() {
        return markers;
    }

    public void setMarkers(List<Marker> markers) {
        this.markers = markers;
    }
}
