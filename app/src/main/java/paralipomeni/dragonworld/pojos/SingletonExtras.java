package paralipomeni.dragonworld.pojos;

import java.util.List;

import paralipomeni.dragonworld.models.User;

//Ho dovuto crere questa Singleton perchè non riesco a pasare una gran mole di dati
//tra un Activity e l'altra. Ho risolto così

public class SingletonExtras {
    private List<User> classifica;

    private static final SingletonExtras instance = new SingletonExtras();

    public static SingletonExtras get() {
        return instance;
    }

    private SingletonExtras() { }

    public List<User> getClassifica() {
        return classifica;
    }

    public void setClassifica(List<User> classifica) {
        this.classifica = classifica;
    }
}
