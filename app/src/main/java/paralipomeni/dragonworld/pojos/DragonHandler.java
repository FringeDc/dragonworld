package paralipomeni.dragonworld.pojos;

import android.os.Handler;

public class DragonHandler extends Handler {

    private DragonRunnable runnable;

    public DragonRunnable getRunnable() {
        return runnable;
    }

    public void setRunnable(DragonRunnable runnable) {
        this.runnable = runnable;
    }

    public void kill() {
        if(this.runnable == null) {
            return;
        }
        super.removeCallbacks(this.runnable);
    }
}
