package paralipomeni.dragonworld.pojos;

import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;

import paralipomeni.dragonworld.models.Sfera;

public class FrecciaSuMappaList extends ArrayList<FrecciaSuMappa> {

    public Sfera getSferaByMarker(Marker marker) {
        if(marker == null) {
            return null;
        }
        for(FrecciaSuMappa frecciaSuMappa : this) {
            if(frecciaSuMappa.getMarker().equals(marker)) {
                return frecciaSuMappa.getSfera();
            }
        }
        return null;
    }

    public void eliminaFrecceSuMappa() {
        for(int i = this.size() -1; i>=0; i--) {
            FrecciaSuMappa frecciaSuMappa = this.get(i);
            frecciaSuMappa.getMarker().remove();
            this.remove(frecciaSuMappa);
        }
    }
}
