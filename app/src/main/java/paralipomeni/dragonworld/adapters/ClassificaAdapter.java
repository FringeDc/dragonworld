package paralipomeni.dragonworld.adapters;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.ColorUtils;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.enums.Country;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.services.UserService;

import static paralipomeni.dragonworld.constants.DragonConstants.Dragonworld.DEFAULT_RESOURCE;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.CIRCLE;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.EMPTY;

public class ClassificaAdapter extends ArrayAdapter<User> {
    private Context context;
    private int viewWidth;
    private List<User> users;

    public ClassificaAdapter(Context context, int ResourceId, List<User> users, int viewWidth) {
        super(context, ResourceId, users);
        this.context = context;
        this.viewWidth = viewWidth;
        this.users = users;
    }

    @NonNull
    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater == null) {
            return convertView;
        }

        User user = getItem(position);
        if (user == null) {
            return convertView;
        }

        if(UserService.isUserInactive(user)) {
            convertView = populateItemForUnactiveUser(user, inflater, position);
        } else {
            convertView = populateItemForActiveUser(user, inflater, position);
        }

        String deviceID = UserService.getDeviceID(context);
        if(user.getDeviceID().equals(deviceID)) {
            final View background = convertView.findViewById(R.id.item_container);

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0.0f, 1.0f);
            valueAnimator.setDuration(1000);
            valueAnimator.setRepeatCount(Animation.INFINITE);
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {

                    float fractionAnim = (float) valueAnimator.getAnimatedValue();

                    background.setBackgroundColor(ColorUtils.blendARGB(
                            context.getResources().getColor(R.color.arancione_trasparente),
                            context.getResources().getColor(R.color.bianco_trasparente),
                            fractionAnim));
                }
            });
            valueAnimator.start();
        }

        return convertView;
    }

    private View populateItemForActiveUser(User user, LayoutInflater inflater, int position) {
        View convertView = inflater.inflate(R.layout.item_active_classifica, null);

        Typeface ubuntu = ResourcesCompat.getFont(context, R.font.ubunturadar);

        TextView posizione = convertView.findViewById(R.id.posizione);
        posizione.setTypeface(ubuntu);
        TextView nome = convertView.findViewById(R.id.nick);
        nome.setTypeface(ubuntu);
        TextView days = convertView.findViewById(R.id.days);
        days.setTypeface(ubuntu);
        TextView prov = convertView.findViewById(R.id.prov);
        days.setTypeface(ubuntu);
        RelativeLayout sfere = convertView.findViewById(R.id.searchsfere);

        ImageView flag = convertView.findViewById(R.id.flag);

        ViewGroup.LayoutParams param = posizione.getLayoutParams();
        param.width = (int) (viewWidth * 0.15);
        posizione.setLayoutParams(param);

        param = nome.getLayoutParams();
        param.width = (int) (viewWidth * 0.45);
        nome.setLayoutParams(param);

        param = prov.getLayoutParams();
        param.width = (int) (viewWidth * 0.15);
        prov.setLayoutParams(param);

        param = sfere.getLayoutParams();
        param.width = (int) (viewWidth * 0.25);
        sfere.setLayoutParams(param);

        String deviceID1 = EMPTY;
        String deviceID2 = EMPTY;
        String deviceID3 = EMPTY;

        if (users.size() > 0) {
            deviceID1 = users.get(0).getDeviceID();
        }
        if (users.size() > 1) {
            deviceID2 = users.get(1).getDeviceID();
        }
        if (users.size() > 2) {
            deviceID3 = users.get(2).getDeviceID();
        }

        String deviceID = user.getDeviceID();

        float fontSize;
        int color;

        if (deviceID.equals(deviceID1)) {
            fontSize = 2.0f;
            color = Color.rgb(247, 202, 39);
        } else if (deviceID.equals(deviceID2)) {
            fontSize = 1.6f;
            color = Color.rgb(153, 153, 153);
        } else if (deviceID.equals(deviceID3)) {
            fontSize = 1.3f;
            color = Color.rgb(219, 145, 122);
        } else {
            fontSize = 1f;
            color = Color.rgb(255, 255, 255);
        }

        RelativeSizeSpan sizeText = new RelativeSizeSpan(fontSize);
        String pos = (position + 1) + CIRCLE;
        Spannable sbPos = new SpannableString(pos);

        sbPos.setSpan(
                sizeText,
                pos.indexOf(pos),
                pos.indexOf(pos) + pos.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        posizione.setText(sbPos);
        posizione.setTextColor(color);

        nome.setText(user.getNickname());
        prov.setText(user.getNazione());

        Country country = Country.getCountryFromString(user.getNazione());
        if(country == null || country.equals(Country.UNKNOWN)) {
            flag.setVisibility(View.GONE);
        } else {
            flag.setImageResource(country.getResource());
        }

        int giorni = UserService.getGiorniDiGioco(user);
        String ricerca;

        if (giorni < 365) {
            ricerca = giorni == 1 ? context.getResources().getString(R.string.item_day) : String.format(context.getString(R.string.item_days), giorni);
        } else {
            int anni = giorni / 365;
            ricerca = anni == 1 ? context.getResources().getString(R.string.item_year) :
                    String.format(context.getString(R.string.item_years), anni);
        }

        days.setText(ricerca);

        TextView wishTv = convertView.findViewById(R.id.wish);
        wishTv.setVisibility(View.GONE);

        int sfereRaccolte = UserService.getQuanteSfereRaccolte(user);
        if (user.getFine() != null || sfereRaccolte >= 7) {
            days.setTextColor(context.getResources().getColor(R.color.arancione));
            days.setTypeface(days.getTypeface(), Typeface.BOLD);

            String wish = user.getWish();

            if(wish != null && !wish.isEmpty()) {
                wishTv.setTypeface(ubuntu);
                wishTv.setVisibility(View.VISIBLE);
                wishTv.setText(wish);
            }
        }

        TypedArray sfereItem = context.getResources().obtainTypedArray(R.array.sfere_adapter_classifica);
        TypedArray raccolteXS = context.getResources().obtainTypedArray(R.array.sfere_raccolte_xs);
        TypedArray nonRaccolteXS = context.getResources().obtainTypedArray(R.array.sfere_nonraccolte_xs);

        for (int i = 0; i < 7; i++) {
            ImageView image = convertView.findViewById(sfereItem.getResourceId(i, DEFAULT_RESOURCE));
            image.setImageResource(nonRaccolteXS.getResourceId(i, DEFAULT_RESOURCE));
            if (user.getSfere().get(i).isFound()) {
                image.setImageResource(raccolteXS.getResourceId(i, DEFAULT_RESOURCE));
            }
        }

        sfereItem.recycle();
        raccolteXS.recycle();
        nonRaccolteXS.recycle();
        return convertView;
    }

    private View populateItemForUnactiveUser(User user, LayoutInflater inflater, int position) {
        View convertView = inflater.inflate(R.layout.item_unactive_classifica, null);

        Typeface ubuntu = ResourcesCompat.getFont(context, R.font.ubunturadar);

        TextView posizione = convertView.findViewById(R.id.posizione);
        posizione.setTypeface(ubuntu);
        TextView nome = convertView.findViewById(R.id.nick);
        nome.setTypeface(ubuntu);
        TextView days = convertView.findViewById(R.id.days);
        days.setTypeface(ubuntu);
        TextView prov = convertView.findViewById(R.id.prov);
        days.setTypeface(ubuntu);

        String pos = (position + 1) + CIRCLE;
        posizione.setText(pos);

        nome.setText(user.getNickname());
        prov.setText(user.getNazione());

        int giorni = UserService.getGiorniDiGioco(user);
        String ricerca;

        if (giorni < 365) {
            ricerca = giorni == 1 ? context.getResources().getString(R.string.item_day) : String.format(context.getString(R.string.item_days), giorni);
        } else {
            int anni = giorni / 365;
            ricerca = anni == 1 ? context.getResources().getString(R.string.item_year) :
                    String.format(context.getString(R.string.item_years), anni);
        }

        days.setText(ricerca);

        return convertView;
    }
}
