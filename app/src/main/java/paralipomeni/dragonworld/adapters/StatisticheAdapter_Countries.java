package paralipomeni.dragonworld.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.enums.Country;

public class StatisticheAdapter_Countries extends ArrayAdapter<String> {
    private Context context;
    private List<String> countries;
    private Map<String, Integer> countriesOccurrence;
    private int giocatoriTotali;

    public StatisticheAdapter_Countries(Context context, int ResourceId,
                                        Map<String, Integer> countriesOccurrence, int giocatoriTotali) {
        super(context, ResourceId, new LinkedList<>(countriesOccurrence.keySet()));
        this.context = context;
        this.countriesOccurrence = countriesOccurrence;
        this.countries = new LinkedList<>(countriesOccurrence.keySet());
        this.giocatoriTotali = giocatoriTotali;
    }

    @NonNull
    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater == null) {
            return convertView;
        }

        convertView = inflater.inflate(R.layout.item_statistiche_country, null);

        String country = countries.get(position);
        Integer quantity = countriesOccurrence.get(country);
        if(quantity == null) {
            quantity = 0;
        }

        Typeface ubuntu = ResourcesCompat.getFont(context, R.font.ubunturadar);

        TextView prov = convertView.findViewById(R.id.prov);
        prov.setTypeface(ubuntu);
        prov.setText(country);

        ImageView flag = convertView.findViewById(R.id.flag);
        Country countryEnum = Country.getCountryFromString(country);
        if(country == null || countryEnum.equals(Country.UNKNOWN)) {
            flag.setVisibility(View.GONE);
        } else {
            flag.setImageResource(countryEnum.getResource());
        }

        TextView users = convertView.findViewById(R.id.stat_users);
        users.setTypeface(ubuntu);
        users.setText(String.format(context.getString(R.string.stat_user4sphere), quantity));

        TextView percentuale = convertView.findViewById(R.id.stat_perc);
        percentuale.setTypeface(ubuntu);
        percentuale.setText(String.format(context.getString(R.string.stat_percentage), 100 * quantity / ((double) giocatoriTotali)));

        return convertView;
    }
}
