package paralipomeni.dragonworld.exceptions;

public class DragonWorldValidationException extends Exception {

    public DragonWorldValidationException(String message) {
        super(message);
    }
}
