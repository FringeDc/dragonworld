package paralipomeni.dragonworld.models;

import com.google.android.gms.maps.model.LatLng;

public class Sfera {

    private boolean found;
    private LatLng latlng;
    private int numeroSfera;

    public boolean isFound() {
        return found;
    }

    public void setFound(boolean found) {
        this.found = found;
    }

    public LatLng getLatlng() {
        return latlng;
    }

    public void setLatlng(LatLng latlng) {
        this.latlng = latlng;
    }

    public int getNumeroSfera() {
        return numeroSfera;
    }

    public void setNumeroSfera(int numeroSfera) {
        this.numeroSfera = numeroSfera;
    }
}
