package paralipomeni.dragonworld.models;

import android.support.annotation.NonNull;

import java.util.Date;
import java.util.List;

import paralipomeni.dragonworld.services.UserService;

public class User implements Comparable<User> {

    private Date inizio;
    private Date fine;
    private String nickname;
    private String citta;
    private String nazione;
    private String deviceID;
    private Boolean toBeUploaded;
    private String wish;
    private List<Sfera> sfere;

    public Date getInizio() {
        return inizio;
    }

    public void setInizio(Date inizio) {
        this.inizio = inizio;
    }

    public Date getFine() {
        return fine;
    }

    public void setFine(Date fine) {
        this.fine = fine;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getNazione() {
        return nazione;
    }

    public void setNazione(String nazione) {
        this.nazione = nazione;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public List<Sfera> getSfere() {
        return sfere;
    }

    public void setSfere(List<Sfera> sfere) {
        this.sfere = sfere;
    }

    public Boolean getToBeUploaded() {
        return toBeUploaded;
    }

    public void setToBeUploaded(Boolean toBeUploaded) {
        this.toBeUploaded = toBeUploaded;
    }

    @Override
    public int compareTo(@NonNull User user) {
        Integer thisSfere = UserService.getQuanteSfereRaccolte(this);
        Integer userSfere = UserService.getQuanteSfereRaccolte(user);

        Integer thisGiorni = UserService.getGiorniDiGioco(this);
        Integer userGiorni = UserService.getGiorniDiGioco(user);

        if(userSfere.equals(thisSfere)) {
            if(thisGiorni.equals(userGiorni)) {
                return this.getInizio().compareTo(user.getInizio());
            }
            return thisGiorni.compareTo(userGiorni);
        }
        return userSfere.compareTo(thisSfere);
    }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }
}
