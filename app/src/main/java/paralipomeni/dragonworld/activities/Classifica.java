package paralipomeni.dragonworld.activities;

import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.adapters.ClassificaAdapter;
import paralipomeni.dragonworld.customizations.DragonActivity;
import paralipomeni.dragonworld.pojos.SingletonExtras;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.services.UserService;

public class Classifica extends DragonActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classifica);

        final List<User> users = SingletonExtras.get().getClassifica();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int viewWidth = size.x;

        //tolgo i margin e il layout delle sfere
        viewWidth = viewWidth - 25 - 105;

        TextView hpos = findViewById(R.id.hpos);
        TextView hname = findViewById(R.id.hname);
        TextView hnaz = findViewById(R.id.hnaz);
        TextView hsfere = findViewById(R.id.hsfere);

        ViewGroup.LayoutParams param = hpos.getLayoutParams();
        param.width = (int)(viewWidth*0.15);
        hpos.setLayoutParams(param);

        param = hname.getLayoutParams();
        param.width = (int)(viewWidth*0.45);
        hname.setLayoutParams(param);

        param = hnaz.getLayoutParams();
        param.width = (int)(viewWidth*0.15);
        hnaz.setLayoutParams(param);

        param = hsfere.getLayoutParams();
        param.width = (int)(viewWidth*0.25);
        hsfere.setLayoutParams(param);

        final ListView listView = findViewById(R.id.classifica);
        ClassificaAdapter adapter = new ClassificaAdapter(getApplicationContext(), R.layout.item_active_classifica, users, viewWidth);
        listView.setAdapter(adapter);

        Button back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        Button myposition = findViewById(R.id.myposition);
        myposition.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String deviceID = UserService.getDeviceID(getApplicationContext());
                if(deviceID == null || deviceID.isEmpty()) {
                    return;
                }
                for (int u=0; u<users.size(); u++) {
                    User user = users.get(u);
                    if(user.getDeviceID() != null && deviceID.equals(user.getDeviceID())) {
                        listView.smoothScrollToPosition(u);
                        break;
                    }
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        finish();
    }

    @Override
    public void onPause() {
        super.onPause();
        finish();
    }
}
