package paralipomeni.dragonworld.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.customizations.DragonActivity;
import paralipomeni.dragonworld.exceptions.DragonWorldValidationException;
import paralipomeni.dragonworld.models.Sfera;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.services.DatabaseService;
import paralipomeni.dragonworld.services.FirebaseService;
import paralipomeni.dragonworld.services.SfereService;
import paralipomeni.dragonworld.services.UserService;
import paralipomeni.dragonworld.validators.InputValidator;

import static paralipomeni.dragonworld.constants.DragonConstants.ActivityConsts.EXTRA_BANNED;
import static paralipomeni.dragonworld.constants.DragonConstants.ActivityConsts.EXTRA_CHART_POSITION;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.EMPTY;

public class Registrati extends DragonActivity {

    private EditText reg_nickname, reg_citta, reg_nazione;
    private TextView message;
    private Button registrati;
    private ProgressBar prog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrati);

        registrati = findViewById(R.id.registrati);
        reg_nickname = findViewById(R.id.reg_nickname);
        reg_citta = findViewById(R.id.reg_citta);
        reg_nazione = findViewById(R.id.reg_nazione);
        prog = findViewById(R.id.bar);
        message = findViewById(R.id.message);

        final DragonActivity activity = this;

        Bundle extras = getIntent().getExtras();
        String bannedUser = null;
        if (extras != null) {
            bannedUser = extras.getString(EXTRA_BANNED);
        }

        if(bannedUser != null && bannedUser.length() > 0) {
            showBannedAlert(bannedUser);
        }

        registrati.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if(imm != null) {
                    imm.hideSoftInputFromWindow(registrati.getWindowToken(), 0);
                }

                String nickname = reg_nickname.getText().toString().trim();
                String citta = reg_citta.getText().toString().trim();
                String nazione = reg_nazione.getText().toString().trim();

                new EsaminaInputGeneraSfere(activity, nickname, citta, nazione).execute();
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class EsaminaInputGeneraSfere extends AsyncTask<String, Void, String> {

        String nickname, citta, nazione, countryCode;
        List<Sfera> customSfere;
        final Context context;
        final DragonActivity activity;

        EsaminaInputGeneraSfere(DragonActivity activity, String nickname, String citta, String nazione) {
            this.activity = activity;
            this.context = activity.getApplicationContext();
            this.nickname = nickname;
            this.citta = citta;
            this.nazione = nazione;
        }

        @Override
        protected void onPreExecute() {
            reg_nickname.setEnabled(false);
            reg_citta.setEnabled(false);
            reg_nazione.setEnabled(false);
            message.setVisibility(View.VISIBLE);
            registrati.setVisibility(View.GONE);
            prog.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                InputValidator.validateNickname(context, nickname);
                InputValidator.validateCitta(context, citta);
                InputValidator.validateNazione(context, nazione);

                Geocoder geoCoder = new Geocoder(activity, Locale.getDefault());
                Address address = geoCoder.getFromLocationName(nazione + ", " + citta, 1).get(0);
                LatLng pos = new LatLng(address.getLatitude(), address.getLongitude());
                countryCode = address.getCountryCode();
                customSfere = SfereService.getSfereOnRandomLocation(pos, 5000);

            } catch (DragonWorldValidationException ex) {
                return ex.getMessage();
            } catch (Exception ex) {
                return String.format(getString(R.string.reg_cannotFindERR), citta, nazione);
            }

            return EMPTY;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equals(EMPTY)) {

                User user = new User();
                user.setToBeUploaded(false);
                user.setInizio(new Date());
                user.setCitta(citta);
                user.setNazione(countryCode);
                user.setNickname(nickname);
                user.setDeviceID(UserService.getDeviceID(context));
                user.setSfere(customSfere);

                FirebaseService.uploadNewUser(activity, user);
            } else {
                activity.toast(result);
            }

            message.setVisibility(View.GONE);
            registrati.setVisibility(View.VISIBLE);
            prog.setVisibility(View.GONE);

            reg_nickname.setEnabled(true);
            reg_citta.setEnabled(true);
            reg_nazione.setEnabled(true);
        }
    }

    private void showBannedAlert(String nickname) {
        Context context = getApplicationContext();

        AlertDialog alert = new AlertDialog.Builder(this)
            .setTitle(String.format(context.getString(R.string.reg_byebye), nickname))
            .setMessage(context.getString(R.string.reg_banned))
            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    dialog.dismiss();
                }
            }).create();

        alert.show();
        alert.getButton(alert.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.radarGreen));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (UserService.isCurrentUserLoggedIn(getApplicationContext())) {
            Intent intent = new Intent(Registrati.this, Radar.class);
            startActivity(intent);
            finish();
        }
    }
}

