package paralipomeni.dragonworld.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.customizations.DragonActivity;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.services.DatabaseService;
import paralipomeni.dragonworld.services.FirebaseService;

public class MakeAwish extends DragonActivity {

    private EditText wish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makeawish);

        wish = findViewById(R.id.editwish);
        Button send = findViewById(R.id.send_wish);

        final DragonActivity activity = this;

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String myWish = wish.getText().toString();

                if (myWish.length() > 10) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MakeAwish.this);
                    builder.setTitle(getString(R.string.wish_sure));
                    builder.setMessage("\"" + myWish + "\"");

                    builder.setPositiveButton(getString(R.string.radar_exit_yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Context context = getApplicationContext();
                            User user = DatabaseService.getCurrentUser(context);
                            if(user == null) {
                                return;
                            }
                            user.setWish(myWish);
                            user.setToBeUploaded(true);

                            FirebaseService.updateUser(activity, user);
                        }
                    });

                    builder.setNegativeButton(getString(R.string.radar_exit_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();
                    alert.getButton(alert.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.radarGreen));
                    alert.getButton(alert.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.radarGreen));
                } else {
                    toast(getString(R.string.wish_length));
                }
            }
        });

        Button back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
    }
}
