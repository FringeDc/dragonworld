package paralipomeni.dragonworld.activities;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.services.ConnectionService;

public class NoConnection extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_connection);

        Button retryConn = findViewById(R.id.btn_retry_conn);
        retryConn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getApplicationContext();
                if(ConnectionService.isNewtworkAvailable(context)) {
                    Intent intent = new Intent(context, SplashScreen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, "alpha", 1f, 0f);
                    objectAnimator.setDuration(500);
                    objectAnimator.setRepeatMode(ObjectAnimator.REVERSE);
                }
            }
        });
    }
}
