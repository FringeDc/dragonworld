package paralipomeni.dragonworld.activities;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.customizations.DragonActivity;
import paralipomeni.dragonworld.models.Sfera;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.services.DatabaseService;
import paralipomeni.dragonworld.services.UserService;

import static paralipomeni.dragonworld.constants.DragonConstants.ActivityConsts.EXTRA_CHART_POSITION;
import static paralipomeni.dragonworld.constants.DragonConstants.Dragonworld.DEFAULT_RESOURCE;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.CIRCLE;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.DASH;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.SIMPLE_DATE_FORMAT;

public class MyProfile extends DragonActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myprofile);

        final User user = DatabaseService.getCurrentUser(getApplicationContext());

        if (user == null) {
            finish();
            return;
        }

        Bundle extras = getIntent().getExtras();
        Integer position = null;
        if (extras != null) {
            position = extras.getInt(EXTRA_CHART_POSITION);
        }

        Button back = findViewById(R.id.tabback);
        TextView nickname = findViewById(R.id.tabnome);
        TextView begin = findViewById(R.id.tabbegin);
        TextView end = findViewById(R.id.tabend);
        TextView pos = findViewById(R.id.tabpos);

        if (position != null) {
            pos.setText(String.format(Locale.getDefault(), "%d%s", position, CIRCLE));
        } else {
            LinearLayout ll = findViewById(R.id.ll_tabpos);
            ll.setVisibility(View.GONE);
        }

        nickname.setText(user.getNickname());

        SimpleDateFormat sdf = new SimpleDateFormat(SIMPLE_DATE_FORMAT, Locale.getDefault());

        Date inizio = user.getInizio();
        if (inizio != null) {
            begin.setText(sdf.format(inizio));
        }
        Date fine = user.getFine();
        if (fine != null) {
            end.setText(sdf.format(fine));
        } else {
            end.setText(DASH);
        }

        TypedArray sfereMyProfile = getResources().obtainTypedArray(R.array.sfere_my_profile);
        TypedArray raccolteXL = getResources().obtainTypedArray(R.array.sfere_raccolte_xl);
        List<Sfera> sfere = user.getSfere();

        for (int i = 0; i < 7; i++) {
            ImageView iv = findViewById(sfereMyProfile.getResourceId(i, DEFAULT_RESOURCE));
            if (sfere.get(i).isFound()) {
                iv.setImageResource(raccolteXL.getResourceId(i, DEFAULT_RESOURCE));
            }
        }

        sfereMyProfile.recycle();
        raccolteXL.recycle();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        int raccolte = UserService.getQuanteSfereRaccolte(user);

        if (raccolte >= 7)
        {
            RelativeLayout tabDetails = findViewById(R.id.tabdetails);
            tabDetails.setBackgroundResource(R.drawable.shenron);

            TextView tabcongrats = findViewById(R.id.tabcongrats);
            tabcongrats.setVisibility(View.VISIBLE);

            RelativeSizeSpan sizeText = new RelativeSizeSpan(1.6f);
            String congratulations = getString(R.string.prog_congr);
            Spannable sbPos = new SpannableString(congratulations);
            sbPos.setSpan(
                    sizeText,
                    congratulations.indexOf(congratulations),
                    congratulations.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            );

            tabcongrats.append(sbPos);

            int days = UserService.getGiorniDiGioco(user);

            String msg = String.format(getString(R.string.prog_congrSub), days);
            Spannable sbPos2 = new SpannableString(msg);
            sbPos2.setSpan(
                    sizeText,
                    msg.indexOf(msg),
                    msg.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            );

            tabcongrats.append(sbPos2);

            Button desire = findViewById(R.id.desire);
            desire.setVisibility(View.GONE);

            TextView wishMade = findViewById(R.id.wishMade);
            wishMade.setVisibility(View.GONE);

            String wish = user.getWish();

            if(wish == null || wish.isEmpty()) {
                desire.setVisibility(View.VISIBLE);
                desire.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getApplicationContext(), MakeAwish.class);
                        startActivity(intent);
                    }
                });
            }
            else {
                wishMade.setVisibility(View.VISIBLE);
                wishMade.setText(wish);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        finish();
    }

    @Override
    public void onPause() {
        super.onPause();
        finish();
    }
}
