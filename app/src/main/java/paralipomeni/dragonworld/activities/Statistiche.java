package paralipomeni.dragonworld.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.adapters.ClassificaAdapter;
import paralipomeni.dragonworld.adapters.StatisticheAdapter_Countries;
import paralipomeni.dragonworld.models.Sfera;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.pojos.SingletonExtras;
import paralipomeni.dragonworld.services.UserService;

import static paralipomeni.dragonworld.constants.DragonConstants.Dragonworld.MILLIS_IN_A_DAY;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.SIMPLE_DATE_FORMAT;

public class Statistiche extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistiche);

        final List<User> users = SingletonExtras.get().getClassifica();

        Date dataInizio = new Date();
        
        int giocatoriTotali = users.size();
        int giocatoriAttivi = 0;
        int giocatoriInattivi = 0;

        int raccolte0Sfere = 0;
        int raccolta1Sfera = 0;
        int raccolte2Sfere = 0;
        int raccolte3Sfere = 0;
        int raccolte4Sfere = 0;
        int raccolte5Sfere = 0;
        int raccolte6Sfere = 0;
        int raccolte7Sfere = 0;

        Map<String, Integer> countries = new HashMap<>();
        
        for(User user : users) {
            boolean isInactive = UserService.isUserInactive(user) ;
            if(isInactive) {
                giocatoriInattivi++;
            } else {
                giocatoriAttivi++;
            }
            
            int sfereRaccolte = UserService.getQuanteSfereRaccolte(user);
            switch (sfereRaccolte) {
                case 0: {raccolte0Sfere++; break;}
                case 1: {raccolta1Sfera++; break;}
                case 2: {raccolte2Sfere++; break;}
                case 3: {raccolte3Sfere++; break;}
                case 4: {raccolte4Sfere++; break;}
                case 5: {raccolte5Sfere++; break;}
                case 6: {raccolte6Sfere++; break;}
                default: {raccolte7Sfere++; break;}
            }


            String nazione = user.getNazione();
            if(nazione != null && !nazione.isEmpty()) {
                Integer perNazione = countries.get(nazione);
                countries.put(nazione, perNazione == null ? 1 : perNazione +1);
            }

            Date inizio = user.getInizio();
            if(inizio != null && inizio.before(dataInizio)) {
                dataInizio = inizio;
            }
        }

        if(!countries.isEmpty()) {
            List<Map.Entry<String, Integer>> entries = new ArrayList<>(countries.entrySet());
            Collections.sort(entries, new Comparator<Map.Entry<String, Integer>>() {
                @Override
                public int compare(Map.Entry<String, Integer> entry1, Map.Entry<String, Integer> entry2) {
                    return entry2.getValue().compareTo(entry1.getValue());
                }
            });

            countries = new LinkedHashMap<>();
            for(Map.Entry<String, Integer> entry : entries) {
                countries.put(entry.getKey(), entry.getValue());
            }
        }

        int giorniTrascorsi = (int) ((new Date().getTime() - dataInizio.getTime()) / MILLIS_IN_A_DAY);
        double iscrittiAlGiorno = (double) giocatoriTotali/giorniTrascorsi;

        double percentualeAttivi = (double) 100 * giocatoriAttivi/giocatoriTotali;
        double percentualeInattivi = (double) 100 * giocatoriInattivi/giocatoriTotali;

        //Grafica

        TextView totali = findViewById(R.id.totali);
        SimpleDateFormat sdf = new SimpleDateFormat(SIMPLE_DATE_FORMAT, Locale.getDefault());
        totali.setText(String.format(getString(R.string.stat_players),
                giocatoriTotali, sdf.format(dataInizio), giorniTrascorsi));

        TextView alGiorno = findViewById(R.id.algiorno);
        alGiorno.setText(String.format(getString(R.string.stat_algiorno), iscrittiAlGiorno));

        TextView dettagli = findViewById(R.id.dettagli);
        dettagli.setText(String.format(getString(R.string.stat_players2),
                giocatoriAttivi, percentualeAttivi, giocatoriInattivi, percentualeInattivi));

        ((TextView) findViewById(R.id.sfera0_utenti))
                .setText(String.format(getString(R.string.stat_user4sphere), raccolte0Sfere));
        ((TextView) findViewById(R.id.sfera0_perc))
                .setText(String.format(getString(R.string.stat_percentage), 100 * raccolte0Sfere/((double) giocatoriTotali)));

        ((TextView) findViewById(R.id.sfera1_utenti))
                .setText(String.format(getString(R.string.stat_user4sphere), raccolta1Sfera));
        ((TextView) findViewById(R.id.sfera1_perc))
                .setText(String.format(getString(R.string.stat_percentage), 100 * raccolta1Sfera/((double) giocatoriTotali)));

        ((TextView) findViewById(R.id.sfera2_utenti))
                .setText(String.format(getString(R.string.stat_user4sphere), raccolte2Sfere));
        ((TextView) findViewById(R.id.sfera2_perc))
                .setText(String.format(getString(R.string.stat_percentage), 100 * raccolte2Sfere/((double) giocatoriTotali)));

        ((TextView) findViewById(R.id.sfera3_utenti))
                .setText(String.format(getString(R.string.stat_user4sphere), raccolte3Sfere));
        ((TextView) findViewById(R.id.sfera3_perc))
                .setText(String.format(getString(R.string.stat_percentage), 100 * raccolte3Sfere/((double) giocatoriTotali)));

        ((TextView) findViewById(R.id.sfera4_utenti))
                .setText(String.format(getString(R.string.stat_user4sphere), raccolte4Sfere));
        ((TextView) findViewById(R.id.sfera4_perc))
                .setText(String.format(getString(R.string.stat_percentage), 100 * raccolte4Sfere/((double) giocatoriTotali)));

        ((TextView) findViewById(R.id.sfera5_utenti))
                .setText(String.format(getString(R.string.stat_user4sphere), raccolte5Sfere));
        ((TextView) findViewById(R.id.sfera5_perc))
                .setText(String.format(getString(R.string.stat_percentage), 100 * raccolte5Sfere/((double) giocatoriTotali)));

        ((TextView) findViewById(R.id.sfera6_utenti))
                .setText(String.format(getString(R.string.stat_user4sphere), raccolte6Sfere));
        ((TextView) findViewById(R.id.sfera6_perc))
                .setText(String.format(getString(R.string.stat_percentage), 100 * raccolte6Sfere/((double) giocatoriTotali)));

        ((TextView) findViewById(R.id.sfera7_utenti))
                .setText(String.format(getString(R.string.stat_user4sphere), raccolte7Sfere));
        ((TextView) findViewById(R.id.sfera7_perc))
                .setText(String.format(getString(R.string.stat_percentage), (double) (100 * raccolte7Sfere/giocatoriTotali)));

        final ListView listView = findViewById(R.id.listview);
        StatisticheAdapter_Countries adapter = new StatisticheAdapter_Countries(getApplicationContext(),
                R.layout.item_active_classifica, countries, giocatoriTotali);
        listView.setAdapter(adapter);


        Button back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
    }
}
