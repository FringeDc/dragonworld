package paralipomeni.dragonworld.activities;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.customizations.DragonActivity;
import paralipomeni.dragonworld.models.Sfera;
import paralipomeni.dragonworld.dtos.SferaDTO;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.populators.SferaPopulator;
import paralipomeni.dragonworld.services.ConnectionService;
import paralipomeni.dragonworld.services.DatabaseService;
import paralipomeni.dragonworld.services.FirebaseService;

import static paralipomeni.dragonworld.constants.DragonConstants.ActivityConsts.EXTRA_SFERA_VICINA;

public class SferaTrovata extends DragonActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sferatrovata);

        if (!ConnectionService.isNewtworkAvailable(getApplicationContext())) {
            Intent intent = new Intent(getApplicationContext(), NoConnection.class);
            startActivity(intent);
            finish();
            return;
        }

        TextView tv = findViewById(R.id.textSfera);
        TextView compl = findViewById(R.id.compl);
        ImageView image = findViewById(R.id.sferaimage);
        final Button back = findViewById(R.id.back);

        Bundle extras = getIntent().getExtras();
        Sfera sfera = null;
        if (extras != null) {
            SferaDTO sferaDTO = (SferaDTO) extras.getSerializable(EXTRA_SFERA_VICINA);
            sfera = SferaPopulator.sferaDTO2sfera(sferaDTO);
        }

        final User user = DatabaseService.getCurrentUser(getApplicationContext());
        if (user == null) {
            finish();
            return;
        }

        if (sfera == null) {
            compl.setText(getString(R.string.find_FAILtitle1));
            tv.setText(getString(R.string.find_FAILsub1));
            image.setImageResource(R.drawable.no_sfera);
            back.setText(getString(R.string.find_back));
        } else {
            int numeroSfera = sfera.getNumeroSfera();
            int posizioneSfera = numeroSfera - 1;

            TypedArray nomiSfere = getResources().obtainTypedArray(R.array.nome_sfera);

            List<Sfera> sfere = user.getSfere();

            if (!sfere.get(posizioneSfera).isFound()) {

                TypedArray raccolteXL = getResources().obtainTypedArray(R.array.sfere_raccolte_xl);

                tv.setText(String.format(getString(R.string.find_SUXsub), nomiSfere.getString(posizioneSfera)));
                image.setImageDrawable(raccolteXL.getDrawable(posizioneSfera));

                sfere.get(posizioneSfera).setFound(true);
                user.setToBeUploaded(true);

                boolean allSfereFound = true;
                for(Sfera uSfera : sfere) {
                    allSfereFound &= uSfera.isFound();
                }

                if(allSfereFound) {
                    user.setFine(new Date());
                }

                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FirebaseService.updateUser(SferaTrovata.this, user);
                    }
                });

                raccolteXL.recycle();
            } else {
                TypedArray nigRaccolteXL = getResources().obtainTypedArray(R.array.sfere_nigraccolte_xl);

                compl.setText(getString(R.string.find_FAILtitle2));
                tv.setText(String.format(getString(R.string.find_FAILsub2), nomiSfere.getString(posizioneSfera)));
                image.setImageDrawable(nigRaccolteXL.getDrawable(posizioneSfera));
                nigRaccolteXL.recycle();


                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
            }
            nomiSfere.recycle();
        }
    }
}
