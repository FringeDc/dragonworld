package paralipomeni.dragonworld.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.customizations.DragonFragmentActivity;
import paralipomeni.dragonworld.models.Sfera;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.pojos.DragonHandler;
import paralipomeni.dragonworld.pojos.DragonRunnable;
import paralipomeni.dragonworld.pojos.FrecciaSuMappa;
import paralipomeni.dragonworld.services.ConnectionService;
import paralipomeni.dragonworld.services.DatabaseService;
import paralipomeni.dragonworld.services.FirebaseService;
import paralipomeni.dragonworld.services.MapService;
import paralipomeni.dragonworld.services.SfereService;
import paralipomeni.dragonworld.services.UserService;

import static paralipomeni.dragonworld.constants.DragonConstants.ActivityConsts.EXTRA_SFERA_VICINA;
import static paralipomeni.dragonworld.constants.DragonConstants.Dragonworld.DEFAULT_RESOURCE;
import static paralipomeni.dragonworld.constants.DragonConstants.Map.MAX_ZOOM;
import static paralipomeni.dragonworld.constants.DragonConstants.Map.MIN_ZOOM;
import static paralipomeni.dragonworld.constants.DragonConstants.Strings.EMPTY;

public class Radar extends DragonFragmentActivity implements OnMapReadyCallback,
        GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveStartedListener {

    private long down;
    private long up;

    private GoogleMap mMap;
    private LatLng startPos;
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(RESULT_CANCELED);
        setContentView(R.layout.activity_radar);

        if (!ConnectionService.isNewtworkAvailable(getApplicationContext())) {
            toastNoConnection();
            finish();
            return;
        }

        checkGraficaSfere();

        TextView title = findViewById(R.id.radarTitle);
        title.setTypeface(ResourcesCompat.getFont(getApplicationContext(), R.font.ubunturadar));
        title.setText(String.format(getString(R.string.radar_title), currentUser().getNickname()));

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        startPos = UserService.getPosizioneCitta(getApplicationContext(), currentUser());
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (!ConnectionService.isNewtworkAvailable(getApplicationContext())) {
            toastNoConnection();
            finish();
            return;
        }

        mMap = googleMap;
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.radar));
        mMap.setMaxZoomPreference(MAX_ZOOM);
        mMap.setMinZoomPreference(MIN_ZOOM);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setIndoorLevelPickerEnabled(false);
        mMap.getUiSettings().setTiltGesturesEnabled(false);

        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveStartedListener(this);

        placeSfere();

        getGPSPos(true);

        placeArrows();

        mMap.animateCamera(CameraUpdateFactory.newLatLng(startPos));

        Button gps = findViewById(R.id.gps);
        gps.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MapService.isGPSEnabled(getApplicationContext()))
                    getGPSPos(true);
            }
        });

        Button profile = findViewById(R.id.profile);
        profile.setTypeface(ResourcesCompat.getFont(getApplicationContext(), R.font.ubunturadar));

        final DragonFragmentActivity activity = this;

        profile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Radar.this, MyProfile.class);
                if (ConnectionService.isNewtworkAvailable(getApplicationContext())) {
                    FirebaseService.getUserProfile(activity);
                } else {
                    startActivity(intent);
                }
            }
        });

        final Button chart = findViewById(R.id.chart);
        chart.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        chart.setBackground(getResources().getDrawable(R.drawable.chart_pressed));
                        down = System.currentTimeMillis();
                        return true;
                    case MotionEvent.ACTION_UP:
                        if (!ConnectionService.isNewtworkAvailable(getApplicationContext())) {
                            toastNoConnection();
                            return false;
                        }

                        up = System.currentTimeMillis();
                        long pressedTime = up - down;
                        if (pressedTime <= 500) {
                            FirebaseService.mostraClassifica(activity);
                        } else {
                            FirebaseService.mostraStatitiche(activity);
                        }
                        chart.setBackground(getResources().getDrawable(R.drawable.chart_unpressed));
                        return true;
                }
                return false;
            }
        });

        Button ray = findViewById(R.id.radarray);
        RotateAnimation rotate = new RotateAnimation(
                0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(1000);
        rotate.setRepeatCount(Animation.INFINITE);
        ray.startAnimation(rotate);

        ray.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(startPos, MIN_ZOOM));
            }
        });

        Button showsfere = findViewById(R.id.showsfere);
        showsfere.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(startPos, MIN_ZOOM));
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                LatLng target;

                Sfera sfera = getFrecceSuMappa().getSferaByMarker(marker);

                //Se clicco su una freccetta
                if (sfera != null) {
                    target = sfera.getLatlng();
                    //Se clicco sulla mia posizione
                } else if (marker.getPosition().equals(myPos)) {
                    return true;
                    //Se clicco su una sfera
                } else {
                    target = marker.getPosition();
                }

                //Se conosco la mia posizione, mostro un toast
                if (myPos != null) {

                    double distance = MapService.distanza(myPos, target);
                    if (distance < 1000) {
                        toast(R.string.radar_distance, distance, "metri");
                    } else {
                        String km = String.valueOf(Math.round(distance / 1000));
                        toast(R.string.radar_distance, km, "km");
                    }
                }
                mMap.animateCamera(CameraUpdateFactory.newLatLng(target));
                return true;
            }
        });

        Button collect = findViewById(R.id.collect);
        collect.setTypeface(ResourcesCompat.getFont(getApplicationContext(), R.font.ubunturadar));
        collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final User user = DatabaseService.getCurrentUser(getApplicationContext());
                if (user == null) {
                    finish();
                    return;
                }

                int raccolte = UserService.getQuanteSfereRaccolte(user);
                if (raccolte >= 7) {
                    Intent intent = new Intent(Radar.this, MyProfile.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(Radar.this, SferaTrovata.class);
                    intent.putExtra(EXTRA_SFERA_VICINA, SfereService.sferaPiuVicina(getApplicationContext()));
                    interstitialAd = ConnectionService.initializeAdInterstitial(getApplicationContext());
                    startActivity(intent);
                }
            }
        });


    }

    private void placeSfere() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        List<Sfera> sfere = currentUser().getSfere();

        TypedArray raccolteXS = getResources().obtainTypedArray(R.array.sfere_raccolte_xs);

        List<Marker> sfereDaLampeggiare = new ArrayList<>();

        for (int i = 0; i < 7; i++) {
            boolean isFound = sfere.get(i).isFound();
            Bitmap source = BitmapFactory.decodeResource(getResources(), isFound ?
                    raccolteXS.getResourceId(i, DEFAULT_RESOURCE) : R.drawable.gen_sfera, options);

            BitmapDescriptor bmpDesc = BitmapDescriptorFactory.fromBitmap(source);
            Marker m = mMap.addMarker(new MarkerOptions()
                    .position(sfere.get(i).getLatlng())
                    .icon(bmpDesc));

            if (!isFound) {
                sfereDaLampeggiare.add(m);
            }
        }

        lampeggia(sfereDaLampeggiare);
        raccolteXS.recycle();
    }

    private void placeArrows() {
        getFrecceSuMappa().eliminaFrecceSuMappa();

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        List<Sfera> sfere = currentUser().getSfere();

        LatLng centro = mMap.getCameraPosition().target;

        for (Sfera sfera : sfere) {
            LatLng sferaPos = sfera.getLatlng();

            double radius = MapService.getRadiusDegrees(mMap);
            LatLng arrowPos = MapService.getArrowPoint(sferaPos, centro, radius);

            if (arrowPos != null && !mMap.getProjection().getVisibleRegion().latLngBounds.contains(sferaPos)) {
                Location locSfere = new Location(EMPTY);
                locSfere.setLatitude(sferaPos.latitude);
                locSfere.setLongitude(sferaPos.longitude);

                Location locCentro = new Location(EMPTY);
                locCentro.setLatitude(centro.latitude);
                locCentro.setLongitude(centro.longitude);

                float bearing = locCentro.bearingTo(locSfere);

                Bitmap source = BitmapFactory.decodeResource(getResources(), R.drawable.triangle, options);
                Bitmap smallMarker = Bitmap.createScaledBitmap(source, 25, 25, false);
                BitmapDescriptor bmpDesc = BitmapDescriptorFactory.fromBitmap(smallMarker);
                Marker marker = mMap.addMarker(new MarkerOptions().position(arrowPos).icon(bmpDesc).rotation(bearing));

                FrecciaSuMappa frecciaSuMappa = new FrecciaSuMappa(sfera, marker);
                getFrecceSuMappa().add(frecciaSuMappa);

                //se la sfera è stata raccolta, il marker è invisibile
                marker.setVisible(!sfera.isFound());
            }
        }
    }

    private void getGPSPos(boolean goTo) {

        LatLng currentPostion = MapService.getGPSPos(getApplicationContext());
        if (currentPostion == null) {
            return;
        }

        myPos = currentPostion;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Bitmap source = BitmapFactory.decodeResource(getResources(), R.drawable.gps, options);
        BitmapDescriptor bmpDesc = BitmapDescriptorFactory.fromBitmap(source);

        removeMyPosition();
        myPosition = mMap.addMarker(new MarkerOptions().position(myPos).icon(bmpDesc));

        if (goTo) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPos, 17));
        }
    }

    private void checkGraficaSfere() {
        TypedArray sfereContorno = getResources().obtainTypedArray(R.array.sfere_contorno_radar);
        TypedArray nonRaccolteXS = getResources().obtainTypedArray(R.array.sfere_nigraccolte_xs);

        List<Sfera> sfere = currentUser().getSfere();

        if (sfere == null || sfere.isEmpty()) {
            return;
        }

        for (int i = 0; i < 7; i++) {
            ImageView iv = findViewById(sfereContorno.getResourceId(i, 0));
            iv.setImageResource(!sfere.get(i).isFound() ?
                    R.drawable.nottaken : nonRaccolteXS.getResourceId(i, 0));
        }

        sfereContorno.recycle();
        nonRaccolteXS.recycle();
    }

    private void lampeggia(final List<Marker> markers) {

        if (markers == null || markers.isEmpty()) {
            return;
        }

        handlerSfere = new DragonHandler();
        DragonRunnable threadLampeggia = new DragonRunnable(markers) {
            @Override
            public void run() {

                boolean isLamped = this.isLamped();

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inScaled = false;

                Bitmap source = BitmapFactory.decodeResource(getResources(), isLamped ?
                        R.drawable.gen_sfera_invisible : R.drawable.gen_sfera, options);

                BitmapDescriptor bmpDesc = BitmapDescriptorFactory.fromBitmap(source);

                for (Marker marker : this.getMarkers()) {
                    marker.setIcon(bmpDesc);
                }

                if (!isLamped) {
                    final MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.bip);
                    mp.start();
                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {
                            mp.release();
                        }
                    });
                }

                setLamped(!isLamped);
                handlerSfere.postDelayed(this, 500);
            }
        };
        handlerSfere.setRunnable(threadLampeggia);
        handlerSfere.postDelayed(threadLampeggia, 500);
    }

    @Override
    public void onCameraIdle() {
        placeArrows();
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        getFrecceSuMappa().eliminaFrecceSuMappa();
    }

    @Override
    public void onResume() {
        super.onResume();
        resumeMapAndThreads();
        ConnectionService.showAdInterstitial(interstitialAd);
    }

    private void resumeMapAndThreads() {
        if (mMap != null) {
            mMap.clear();
            getGPSPos(false);
            startPos = UserService.getPosizioneCitta(getApplicationContext(), currentUser());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(startPos, MIN_ZOOM));

            placeSfere();
            checkGraficaSfere();
        }
    }
}
