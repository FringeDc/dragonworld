package paralipomeni.dragonworld.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import paralipomeni.dragonworld.R;
import paralipomeni.dragonworld.customizations.DragonActivity;
import paralipomeni.dragonworld.models.User;
import paralipomeni.dragonworld.services.DatabaseService;
import paralipomeni.dragonworld.services.FirebaseService;

import static paralipomeni.dragonworld.constants.DragonConstants.Dragonworld.PERMISSIONS_REQUEST_LOCATION;

public class SplashScreen extends DragonActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                checkLocationPermission();
            }
        }, 2000);
    }

    private void checkLocationPermission()
    {
        int hasPermission = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_LOCATION);

        } else {
            goToMainActivity();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSIONS_REQUEST_LOCATION && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            goToMainActivity();
        }

        int hasPermission = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.ACCESS_FINE_LOCATION);

        if(hasPermission !=  PackageManager.PERMISSION_GRANTED) {
            toast(R.string.radar_allowPosition);
            finish();
        }
    }

    private void goToMainActivity() {
        User currentUser = DatabaseService.getCurrentUser(getApplicationContext());
        DragonActivity activity = this;

        if (currentUser == null) {
            FirebaseService.downloadUser(activity);
        } else {
            boolean toBeUploaded = Boolean.TRUE.equals(currentUser.getToBeUploaded());
            boolean uploaded = true;
            if (toBeUploaded) {
                uploaded = FirebaseService.updateUser(activity, currentUser);
            }
            if (!toBeUploaded || !uploaded) {
                Intent intent = new Intent(activity, Radar.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
    }
}
